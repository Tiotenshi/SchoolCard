﻿using SchoolCard.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace SchoolCard.ViewModels
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Post> CardList { get; set; }
        public INavigation Navigation { get; set; }

        public MainPageViewModel()
        {
            //CardList = new ObservableCollection<Post>
            //{
            //    new Cardholder()
            //    {
            //        Name="Иванов Иван", Text="совершен вход по карте", Time="13:19", Day="16/04/2018"
            //    },
            //    new Cardholder()
            //    {
            //        Name="Иванов Иван", Text="совершен вход по карте", Time="13:19", Day="16/04/2018"
            //    },
            //    new Cardholder()
            //    {
            //        Name="Иванов Иван", Text="совершен вход по карте", Time="13:19", Day="16/04/2018"
            //    },
            //    new Cardholder()
            //    {
            //        Name="Иванов Иван", Text="совершен вход по карте", Time="13:19", Day="16/04/2018"
            //    },
            //    new Cardholder()
            //    {
            //        Name="Иванов Иван", Text="совершен вход по карте", Time="13:19", Day="16/04/2018"
            //    },
            //    new Cardholder()
            //    {
            //        Name="Иванов Иван", Text="совершен вход по карте", Time="13:19", Day="16/04/2018"
            //    },
            //    new Cardholder()
            //    {
            //        Name="Иванов Иван", Text="совершен вход по карте", Time="13:19", Day="16/04/2018"
            //    },
            //    new Cardholder()
            //    {
            //        Name="Иванов Иван", Text="совершен вход по карте", Time="13:19", Day="16/04/2018"
            //    }
            //};
        }

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
        #endregion
    }
}
