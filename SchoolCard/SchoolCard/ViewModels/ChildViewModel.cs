﻿using Plugin.FirebasePushNotification;
using SchoolCard.Models;
using SchoolCard.Services;
using SchoolCard.ViewModels.Commands;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SchoolCard.ViewModels
{
    public class ChildViewModel : INotifyPropertyChanged
    {
        #region ИНИТЫ
        private ObservableCollection<Children> childrenList;
        public ObservableCollection<Children> ChildrenList
        {
            get { return childrenList; }
            set { childrenList = value; OnPropertyChanged(); }
        }

        private bool visibleList;
        public bool VisibleList
        {
            get { return visibleList; }
            set { visibleList = value; OnPropertyChanged(); }
        }

        #endregion

        #region КОМАНДЫ
        public DeleteChildCommand DeleteChildCommand { get; set; }
        public async void DeleteButton(Children child)
        {
            var response = await RestServices.DeleteChild(child);

            if (response)
            {
                using (SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
                {
                    var list = conn.Table<Children>().ToList();

                    var ch = list.FirstOrDefault(x => x.name == child.name);

                    int rows = conn.Delete(ch);
                    if (rows > 0)
                        await App.Current.MainPage.DisplayAlert("Успешно!", "Запись удалена.", "Ок");
                    else
                        await App.Current.MainPage.DisplayAlert("Не выполнено!", "Ошибка удаления.", "Ок");

                    list.Remove(ch);
                    
                    ChildrenList = new ObservableCollection<Children>(list);
                    if (ChildrenList.Count == 0)
                        VisibleList = false;
                    else VisibleList = true;
                }
            }
            else
                await App.Current.MainPage.DisplayAlert("Не выполнено!", "Ошибка удаления на сервере.", "Ок");

        }
        #endregion

        #region CTOR
        public ChildViewModel()
        {
            ChildrenList = new ObservableCollection<Children>();

            DeleteChildCommand = new DeleteChildCommand(this);
        }
        #endregion

        #region МЕТОДЫ
        public void OnAppear()
        {
            using (SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
            {
                conn.CreateTable<Children>();
                var list = conn.Table<Children>().ToList();
                if (list.Count == 0)
                    VisibleList = false;
                else VisibleList = true;

                ChildrenList.Clear();
                foreach (var ch in list)
                    ChildrenList.Add(ch);
            }
        }

        public async void AddButton(string name, string card)
        {
            Children children = new Children()
            {
                name = name,
                card = card,
                token = CrossFirebasePushNotification.Current.Token
            };

            var response = await RestServices.PostChild(children);

            if (response)
            {
                using (SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
                {
                    conn.CreateTable<Children>();
                    int rows = conn.Insert(children);

                    if (rows > 0)
                        await App.Current.MainPage.DisplayAlert("Успешно!", "Запись добавлена.", "Ок");
                    else
                        await App.Current.MainPage.DisplayAlert("Не выполнено!", "Запись не добавлена.", "Ок");

                    var list = conn.Table<Children>().ToList();
                    VisibleList = true;

                    ChildrenList.Clear();
                    foreach (var ch in list)
                    {
                        ChildrenList.Add(ch);
                    }
                }
            }
            else
                await App.Current.MainPage.DisplayAlert("Не выполнено!", "Не удалось связаться с сервером.", "Ок");
        }

        
        #endregion

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
