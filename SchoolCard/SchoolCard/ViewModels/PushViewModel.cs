﻿using SchoolCard.Helpers;
using SchoolCard.Models;
using SchoolCard.Services;
using SchoolCard.ViewModels.Commands;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SchoolCard.ViewModels
{
    public class PushViewModel : INotifyPropertyChanged
    {
        #region ИНИТЫ
        private bool visibleList;
        public bool VisibleList
        {
            get { return visibleList; }
            set { visibleList = value; OnPropertyChanged(); }
        }

        private List<string> childrens;
        public List<string> Childrens
        {
            get { return childrens; }
            set { childrens = value; OnPropertyChanged(); }
        }

        private ObservableCollection<Post> postList;
        public ObservableCollection<Post> PostList
        {
            get { return postList; }
            set { postList = value; OnPropertyChanged(); }
        }
        //private ObservableCollection<Post> filterList;
        //public ObservableCollection<Post> FilterList
        //{
        //    get { return filterList; }
        //    set { filterList = value; OnPropertyChanged(); }
        //}

        private string filterName;
        public string FilterName
        {
            get { return filterName; }
            set { filterName = value; OnPropertyChanged(); }
        }

        #endregion

        #region КОМАНДЫ
        public DeletePostCommand DeletePostCommand { get; set; }
        public async void DeleteButton(Post post)
        {
            var response = await RestServices.DeletePush(post);

            if (response)
            {
                using (SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
                {
                    var list = conn.Table<Post>().ToList();
                    list.Reverse();

                    Post p = list.FirstOrDefault(x => x.card == post.card && x.day == post.day && x.time == post.time && x.direction == post.direction && x.name == post.name);

                    int rows = conn.Delete(p);
                    if (rows > 0)
                        await App.Current.MainPage.DisplayAlert("Успешно!", "Запись удалена.", "Ок");
                    else
                        await App.Current.MainPage.DisplayAlert("Не выполнено!", "Ошибка удаления.", "Ок");

                    list.Remove(p);
                    
                    if (FilterName != "Показать все")
                    {
                        var selectedNames = from n in list
                                            where n.name == FilterName
                                            select n;
                        PostList.Clear();
                        foreach (var s in selectedNames)
                            PostList.Add(s);
                    }
                    else
                    {
                        PostList = new ObservableCollection<Post>(list);
                    }
                    
                    if (PostList.Count == 0)
                        VisibleList = false;
                    else VisibleList = true;
                }
            }
            else
                await App.Current.MainPage.DisplayAlert("Не выполнено!", "Ошибка удаления на сервере.", "Ок");
        }

        public ICommand FilterCommand { get; protected set; }
        public async void Filter_Command()
        {
            using (SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
            {
                conn.CreateTable<Children>();
                var childrenList = conn.Table<Children>().ToList();
                var names = new List<string>();
                names.Add("Показать все");
                foreach (var n in childrenList)
                    names.Add(n.name);

                FilterName = await App.Current.MainPage.DisplayActionSheet("Фильтр", null, null, names.ToArray());
                conn.CreateTable<Post>();
                var list = conn.Table<Post>().ToList();
                list.Reverse();
                if (FilterName != "Показать все")
                {
                    var selectedNames = from n in list
                                        where n.name == FilterName
                                        select n;
                    PostList.Clear();
                    foreach (var p in selectedNames)
                        PostList.Add(p);
                    //FilterList = PostList;
                }
                else
                {
                    PostList.Clear();
                    foreach (var p in list)
                        PostList.Add(p);
                }

                if (PostList.Count == 0)
                    VisibleList = false;
                else VisibleList = true;

            }
        }
        #endregion

        #region CTOR
        public PushViewModel()
        {
            PostList = new ObservableCollection<Post>();
            //FilterList = new ObservableCollection<Post>();
            childrens = new List<string>();

            DeletePostCommand = new DeletePostCommand(this);
            FilterCommand = new Command(Filter_Command);
            FilterName = "Показать все";
        }
        #endregion

        #region МЕТОДЫ
        public async void OnAppear()
        {
            using (SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
            {
                conn.CreateTable<Post>();
                var list = conn.Table<Post>().ToList();
                list.Reverse();
                

                PostList.Clear();
                foreach (var p in list)
                    PostList.Add(p);

                conn.CreateTable<Children>();
                var childrenList = conn.Table<Children>().ToList();
                Childrens.Clear();
                foreach (var ch in childrenList)
                {
                    Childrens.Add(ch.name);
                    var newposts = await RestServices.GetBouquets(ch.card);
                    if (newposts != null)
                    {
                        foreach (var p in newposts)
                        {
                            p.card = ch.card;
                            p.name = ch.name;
                            bool response = await AddToDB.AddToDatabase(p);
                            if (response)
                                PostList.Insert(0, p);
                        }
                    }
                }
                if (PostList.Count == 0)
                    VisibleList = false;
                else VisibleList = true;
            }
        }
        #endregion

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
