﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace SchoolCard.ViewModels.Commands
{
    public class DeletePostCommand : ICommand
    {
        PushViewModel ViewModel;

        public DeletePostCommand(PushViewModel viewModel)
        {
            this.ViewModel = viewModel;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            var post = (Models.Post)parameter;

            ViewModel.DeleteButton(post);
        }
    }
}
