﻿using SchoolCard.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace SchoolCard.ViewModels.Commands
{
    public class DeleteChildCommand : ICommand
    {
        ChildViewModel ViewModel;

        public DeleteChildCommand(ChildViewModel viewModel)
        {
            this.ViewModel = viewModel;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            var Child = (Children)parameter;
            
            ViewModel.DeleteButton(Child);
        }
    }
}
