﻿using SchoolCard.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolCard.Helpers
{
    public class AddToDB
    {
        public static async Task<bool> AddToDatabase(Post post)
        {
            using (SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
            {
                conn.CreateTable<Post>();

                var list = conn.Table<Post>().ToList();
                var search = list.FirstOrDefault(x => x.card == post.card && x.day == post.day && x.time == post.time && x.direction == post.direction && x.name == post.name);

                if (search == null)
                {
                    int rows = conn.Insert(post);
                    if (rows > 0)
                        return true;
                    else return false;
                }
                return false;
            }
        }
    }
}
