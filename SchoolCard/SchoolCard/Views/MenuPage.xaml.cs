﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SchoolCard.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuPage : MasterDetailPage
    {
		public MenuPage ()
		{
			InitializeComponent ();

            Detail = new NavigationPage(new PushPage())
            {
                BarBackgroundColor = Color.FromHex("#2987b6"),
                BarTextColor = Color.White
            };
		}

        private void PushButton_Clicked(object sender, EventArgs e)
        {
            Detail = new NavigationPage(new PushPage())
            {
                BarBackgroundColor = Color.FromHex("#2987b6"),
                BarTextColor = Color.White
            };

            IsPresented = false;
        }

        private void ChildButton_Clicked(object sender, EventArgs e)
        {
            Detail = new NavigationPage(new ChildPage())
            {
                BarBackgroundColor = Color.FromHex("#2987b6"),
                BarTextColor = Color.White
            };

            IsPresented = false;
        }
    }
}