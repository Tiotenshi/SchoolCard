﻿
using SchoolCard.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SchoolCard.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChildPage : ContentPage
	{
        ChildViewModel viewModel;

		public ChildPage ()
		{
			InitializeComponent ();

            viewModel = new ChildViewModel();
            BindingContext = viewModel;
		}

        private void AddButton_Clicked(object sender, System.EventArgs e)
        {

            if(!string.IsNullOrWhiteSpace(nameEntry.Text) && !string.IsNullOrWhiteSpace(cardEntry.Text))
            {
                var cardText = cardEntry.Text.Replace(",","");
                cardText = cardText.Replace(".", "");
                if (cardText.StartsWith("0"))
                    cardText = cardText.Remove(0, 1);
                viewModel.AddButton(nameEntry.Text, cardText);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            viewModel.OnAppear();
        }
    }
}