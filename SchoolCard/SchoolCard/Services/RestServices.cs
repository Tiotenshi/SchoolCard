﻿using Newtonsoft.Json;
using Plugin.FirebasePushNotification;
using SchoolCard.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SchoolCard.Services
{
    public class RestServices
    {
        public static async Task<bool> PostChild(Children child)
        {
            var url = "http://188.225.82.213/login";

            try
            {
                var json = JsonConvert.SerializeObject(child);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpClient client = new HttpClient();
                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<bool> DeleteChild(Children child)
        {
            var url = "http://188.225.82.213/delete-card";
            try
            {
                var json = JsonConvert.SerializeObject(child);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpClient client = new HttpClient();
                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<bool> DeletePush(Post post)
        {
            var url = "http://188.225.82.213/delete-push";

            try
            {
                var json = JsonConvert.SerializeObject(post);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpClient client = new HttpClient();
                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
           
        }

        public static async Task<List<Post>> GetBouquets(string card/*, string name*/)
        {
            var url = string.Format("http://188.225.82.213/getpushes?card={0}", card);

            try
            {
                HttpClient client = new HttpClient();

                var response = await client.GetAsync(url);

                var json = await response.Content.ReadAsStringAsync();
                
                var root = JsonConvert.DeserializeObject<ListPost>(json);

                if (root.push == null)
                    return null;
                else
                    return root.push as List<Post>;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
