﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolCard.Models
{
    public class Children
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }

        public string card { get; set; }

        public string name { get; set; }

        public string token { get; set; }
    }
}
