﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolCard.Models
{
    public class Post
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }

        public string card { get; set; }
        
        public string name { get; set; }

        public string direction { get; set; }

        public string time { get; set; }

        public string day { get; set; }
    }

    public class ListPost
    {
        public IList<Post> push { get; set; }
        public string error { get; set; }
    }
    
}
