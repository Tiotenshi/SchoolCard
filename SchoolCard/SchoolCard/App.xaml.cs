﻿using Plugin.FirebasePushNotification;
using SchoolCard.Helpers;
using SchoolCard.Models;
using SchoolCard.Views;
using SQLite;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace SchoolCard
{
    public partial class App : Application
    {
        public static string DatabaseLocation = string.Empty;

        public App()
        {
            InitializeComponent();

            MainPage = new LoginPage();

            System.Diagnostics.Debug.WriteLine($"TOKEN : {CrossFirebasePushNotification.Current.Token}");

            FireBaseInit();
        }

        public App(string databaseLocation)
        {
            InitializeComponent();

            MainPage = new MenuPage();

            DatabaseLocation = databaseLocation;

            System.Diagnostics.Debug.WriteLine($"TOKEN : {CrossFirebasePushNotification.Current.Token}");

            FireBaseInit();
        }

        public void FireBaseInit()
        {
            CrossFirebasePushNotification.Current.OnTokenRefresh += (s, p) =>
            {
                System.Diagnostics.Debug.WriteLine($"TOKEN : {p.Token}");
            };

            CrossFirebasePushNotification.Current.OnNotificationReceived += (s, p) =>
            {
                System.Diagnostics.Debug.WriteLine("Сообщение получено");
            };

            CrossFirebasePushNotification.Current.OnNotificationOpened += (s, p) =>
            {
                System.Diagnostics.Debug.WriteLine("Сообщение открыто");

                //if (p.Data != null && p.Data.Count != 0 && p != null)
                //{
                //    Post post = new Post();
                //    p.Data.TryGetValue("Name", out var n);
                //    if (n != null)
                //        post.Name = (string)n;

                //    p.Data.TryGetValue("Card", out var c);
                //    if (c != null)
                //        post.Card = (string)c;

                //    p.Data.TryGetValue("Time", out var t);
                //    if (t != null)
                //        post.Time = (string)t;

                //    p.Data.TryGetValue("Day", out var d);
                //    if (d != null)
                //        post.Day = (string)d;

                //    p.Data.TryGetValue("Direction", out var dir);
                //    if (dir != null)
                //        post.Direction = (string)dir;

                //    if (post.Name != null && post.Card != null && post.Time != null && post.Day != null && post.Direction != null)
                //    {
                //        AddToDB.AddToDatabase(post);
                //    }
                //}

            };

            CrossFirebasePushNotification.Current.OnNotificationAction += (s, p) =>
            {
                System.Diagnostics.Debug.WriteLine("Action");

                if (!string.IsNullOrEmpty(p.Identifier))
                {
                    System.Diagnostics.Debug.WriteLine($"ActionId: {p.Identifier}");
                    foreach (var data in p.Data)
                    {
                        System.Diagnostics.Debug.WriteLine($"{data.Key} : {data.Value}");
                    }
                }
            };
        }
        

        //public void LoginButton()
        //{
        //    MainPage = new MenuPage();
        //}

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
        }
    }
}
