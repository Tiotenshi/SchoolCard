﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;

namespace SchoolCard.Droid
{
    public class PushNotificationImplementation
    {
        private static int idNotification = 0;
        public bool Send(string title, string message, string namePage = null)
        {
            try
            {
                Context context = Android.App.Application.Context;

                var soundUri = RingtoneManager.GetActualDefaultRingtoneUri(context, RingtoneType.Notification);
                var iconResource = context.ApplicationInfo.Icon;

                Intent resultIntent = new Intent(context, typeof(MainActivity));
                if (namePage != null)
                    resultIntent.PutExtra("PageForOpen", namePage);


                PendingIntent resultPendingIntent = PendingIntent.GetActivity(context, 0, resultIntent, PendingIntentFlags.OneShot | PendingIntentFlags.UpdateCurrent);


                var builder = new NotificationCompat.Builder(context)
                    .SetAutoCancel(false)
                    .SetPriority(NotificationCompat.PriorityHigh)
                    .SetFullScreenIntent(resultPendingIntent, false)
                    .SetContentIntent(resultPendingIntent)
                    .SetContentTitle(title)
                    .SetSound(soundUri)
                    .SetSmallIcon(Resource.Drawable.SC_launcher)
                    .SetVibrate(new long[] { 500, 1000 })
                    .SetContentText(message);

                if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.JellyBean)
                {
                    var style = new NotificationCompat.BigTextStyle();
                    style.BigText(message);
                    builder.SetStyle(style);
                }

                NotificationManager notificationManager = context.GetSystemService(Context.NotificationService) as NotificationManager;

                notificationManager.Notify(idNotification++, builder.Build());

                return true;

            }
            catch (Java.Lang.Exception ex)
            {
                //Crashes.TrackError(ex);
                var s = ex.Message;
                return false;
            }
        }
    }
}