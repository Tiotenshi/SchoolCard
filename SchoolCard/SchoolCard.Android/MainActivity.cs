﻿
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Plugin.FirebasePushNotification;
using System;
using System.IO;

namespace SchoolCard.Droid
{
    [Activity(Label = "Школьная карта", Icon = "@drawable/SC_launcher", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, LaunchMode = LaunchMode.SingleTop)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            string dbName = "SchoolCard_db.sqlite";
            string folderPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string fullPath = Path.Combine(folderPath, dbName);
            

            LoadApplication(new App(fullPath));

            FirebasePushNotificationManager.ProcessIntent(this, Intent);
        }

        //protected override void OnNewIntent(Intent intent)
        //{
        //    //base.OnNewIntent(intent);
        //    var s = intent.GetStringExtra("MenuPage");
        //}


    }
}